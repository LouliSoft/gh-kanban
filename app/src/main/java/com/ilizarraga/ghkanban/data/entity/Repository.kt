package com.ilizarraga.ghkanban.data.entity

import java.io.Serializable

data class Repository(val id: Int, val name: String, val private: Boolean, val owner: Owner) : Serializable