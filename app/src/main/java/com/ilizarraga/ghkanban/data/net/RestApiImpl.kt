package com.ilizarraga.ghkanban.data.net

import com.ilizarraga.ghkanban.data.entity.RepositoriesDataWrapper
import io.reactivex.Observable

class RestApiImpl : RestApi {

    override fun getGHRepositories() : Observable<RepositoriesDataWrapper> {
        return ApiConnection.getApiService().getGHRepositories()
    }
}