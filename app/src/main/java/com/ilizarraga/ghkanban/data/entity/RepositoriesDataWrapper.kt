package com.ilizarraga.ghkanban.data.entity

data class RepositoriesDataWrapper(val data: RepositoriesDataContainer)