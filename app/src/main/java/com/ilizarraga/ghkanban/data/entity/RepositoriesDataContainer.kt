package com.ilizarraga.ghkanban.data.entity

data class RepositoriesDataContainer(val results: List<Repository>)