package com.ilizarraga.ghkanban.data.entity.mapper

import com.ilizarraga.ghkanban.data.entity.RepositoriesDataWrapper
import com.ilizarraga.ghkanban.domain.entity.Repository

class RepositoriesDataMapper {

    fun transform (repositoriesDataWrapper: RepositoriesDataWrapper) : List<Repository> {
        val repositories: ArrayList<Repository> = ArrayList()
        for (repositoriesData in repositoriesDataWrapper.data.results) {
            val repository = Repository(repositoriesData.id, repositoriesData.name, repositoriesData.private, repositoriesData.owner.login)
            repositories.add(repository)
        }
        return repositories
    }
}