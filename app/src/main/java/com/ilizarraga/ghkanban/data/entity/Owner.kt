package com.ilizarraga.ghkanban.data.entity

data class Owner(val login: String)