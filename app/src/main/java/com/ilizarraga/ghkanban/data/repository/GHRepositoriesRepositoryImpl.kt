package com.ilizarraga.ghkanban.data.repository

import com.ilizarraga.ghkanban.data.entity.mapper.RepositoriesDataMapper
import com.ilizarraga.ghkanban.data.net.RestApi
import com.ilizarraga.ghkanban.data.net.RestApiImpl
import com.ilizarraga.ghkanban.domain.entity.Repository
import com.ilizarraga.ghkanban.domain.repository.GHRepositoriesRepository
import io.reactivex.Observable

class GHRepositoriesRepositoryImpl : GHRepositoriesRepository{

    private var restApi: RestApi = RestApiImpl()
    private val repositoriesDataMapper: RepositoriesDataMapper = RepositoriesDataMapper()

    override fun getRepositoryList() : Observable<List<Repository>> {
        return restApi.getGHRepositories().map(this.repositoriesDataMapper::transform)
    }
}