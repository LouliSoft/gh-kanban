package com.ilizarraga.ghkanban.data.net

import com.ilizarraga.ghkanban.data.entity.RepositoriesDataWrapper
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers

interface RestApi {

    @Headers("Accept: application/vnd.github.v3+json",
            "Content-Type: application/json"
    )
    @GET("/repositories")
    fun getGHRepositories() : Observable<RepositoriesDataWrapper>
}