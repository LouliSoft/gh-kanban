package com.ilizarraga.ghkanban.domain.repository

import com.ilizarraga.ghkanban.domain.entity.Repository
import io.reactivex.Observable

interface GHRepositoriesRepository {

    fun getRepositoryList() : Observable<List<Repository>>
}