package com.ilizarraga.ghkanban.domain.usecase

import com.ilizarraga.ghkanban.domain.entity.Repository
import com.ilizarraga.ghkanban.domain.repository.GHRepositoriesRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetRepositoryListUseCase(private val gHRepositoriesRepository: GHRepositoriesRepository) {


    fun execute() : Observable<List<Repository>> {
        return buildUseCaseObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun buildUseCaseObservable() : Observable<List<Repository>> {
        return gHRepositoriesRepository.getRepositoryList()
    }
}