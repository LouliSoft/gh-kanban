package com.ilizarraga.ghkanban.domain.entity

import java.io.Serializable

data class Repository(val id: Int, val name: String, val private: Boolean, val userName: String): Serializable