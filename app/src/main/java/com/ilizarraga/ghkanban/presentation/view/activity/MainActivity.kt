package com.ilizarraga.ghkanban.presentation.view.activity

import android.os.Bundle
import com.ilizarraga.ghkanban.R
import com.ilizarraga.ghkanban.presentation.view.fragment.MainFragment

class MainActivity : BaseActivity() {

    private var mainFragment: MainFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainFragment = supportFragmentManager.findFragmentById(R.id.main_container) as MainFragment?

        if (mainFragment == null) {
            mainFragment = MainFragment().newInstance()

            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.main_container, mainFragment)
                    .commit()
        }
    }
}
