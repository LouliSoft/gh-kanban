package com.ilizarraga.ghkanban.presentation.presenter

import io.reactivex.disposables.Disposable

interface BasePresenter {

    companion object {
        lateinit var disposable : Disposable
    }

    fun start()

    fun clearObservables() {
        disposable.dispose()
    }
}