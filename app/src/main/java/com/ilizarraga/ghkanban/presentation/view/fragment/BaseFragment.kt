package com.ilizarraga.ghkanban.presentation.view.fragment

import android.support.v4.app.Fragment

open class BaseFragment : Fragment()