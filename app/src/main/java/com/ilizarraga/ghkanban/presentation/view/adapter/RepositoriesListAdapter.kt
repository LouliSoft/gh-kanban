package com.ilizarraga.ghkanban.presentation.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.ilizarraga.ghkanban.R
import com.ilizarraga.ghkanban.domain.entity.Repository
import kotlinx.android.synthetic.main.list_row.view.*


class RepositoriesListAdapter(private val context: Context?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var repositories: ArrayList<Repository> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_row, parent, false))
    }

    override fun onBindViewHolder(holder:RecyclerView.ViewHolder, position: Int) {
        val animation : Animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)

        holder.itemView.startAnimation(animation)

        (holder as ViewHolder).bind(repositories[position])
    }

    override fun getItemCount(): Int =repositories.size

    fun addAll (repositories: List<Repository>) : List<Repository> {
        for (repository: Repository in repositories) {
            add(repository)
        }

        return repositories
    }

    fun add (repository: Repository) {
        repositories.add(repository)
        notifyItemInserted(repositories.size - 1)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Repository) {
            itemView.list_row_name_text_view.text = item.name
            itemView.list_row_username_text_view.text = item.userName
        }
    }
}