package com.ilizarraga.ghkanban.presentation.view.fragment

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ilizarraga.ghkanban.R
import com.ilizarraga.ghkanban.data.repository.GHRepositoriesRepositoryImpl
import com.ilizarraga.ghkanban.domain.entity.Repository
import com.ilizarraga.ghkanban.domain.repository.GHRepositoriesRepository
import com.ilizarraga.ghkanban.presentation.presenter.MainPresenter
import com.ilizarraga.ghkanban.presentation.presenter.contract.MainContract
import com.ilizarraga.ghkanban.presentation.view.adapter.RepositoriesListAdapter
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment(), MainContract.View {

    private lateinit var adapter: RepositoriesListAdapter
    private lateinit var linearLayoutManager : LinearLayoutManager

    override lateinit var mPresenter: MainContract.Presenter

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_explore -> {
                mPresenter.getRepositoriesList()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_local -> {
                mPresenter.getKanbanList()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    init {
        val gHRepositoriesRepository : GHRepositoriesRepository = GHRepositoriesRepositoryImpl()
        mPresenter = MainPresenter(this, gHRepositoriesRepository)
    }

    fun newInstance() : MainFragment {
        return MainFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_main_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        linearLayoutManager = LinearLayoutManager(activity)
        fragment_main_recycler_view.layoutManager = linearLayoutManager

        adapter = RepositoriesListAdapter(context)
        fragment_main_recycler_view.adapter = adapter

        fragment_main_recycler_view.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))

        mPresenter.start()
    }

    override fun showRepositoriesList(repositoriesList: List<Repository>) {
        val currentListSize = adapter.itemCount

        adapter.addAll(repositoriesList)

        fragment_main_recycler_view.adapter.notifyItemRangeInserted(currentListSize, repositoriesList.size)
    }

    override fun showError(error: Throwable) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}