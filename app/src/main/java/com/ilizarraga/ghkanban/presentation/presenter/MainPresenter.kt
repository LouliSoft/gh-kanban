package com.ilizarraga.ghkanban.presentation.presenter

import com.ilizarraga.ghkanban.domain.entity.Repository
import com.ilizarraga.ghkanban.domain.repository.GHRepositoriesRepository
import com.ilizarraga.ghkanban.domain.usecase.GetRepositoryListUseCase
import com.ilizarraga.ghkanban.presentation.presenter.contract.MainContract
import io.reactivex.Observable
import java.net.SocketTimeoutException
import java.util.ArrayList

class MainPresenter(private val view: MainContract.View, gHRepositoriesRepository: GHRepositoriesRepository) : MainContract.Presenter {

    private var getRepositoryListUseCase: GetRepositoryListUseCase = GetRepositoryListUseCase(gHRepositoriesRepository)
    lateinit var observable : Observable<List<Repository>>

    override fun start() {
        getRepositoriesList()
    }

    override fun getRepositoriesList() {
        observable = getRepositoryListUseCase.execute()
        doSubscribe()
    }

    override fun getKanbanList() {

    }

    private fun doSubscribe() {
        BasePresenter.disposable = observable.subscribe({ result -> onSuccess(result)}, { error -> onError(error)})
    }

    private fun onSuccess(data: List<Repository>) {
        view.showRepositoriesList(data)
    }

    private fun onError(error: Throwable) {
        if (error is SocketTimeoutException)
        // subscribe again
            doSubscribe()
        else
            view.showError(error)
    }

}