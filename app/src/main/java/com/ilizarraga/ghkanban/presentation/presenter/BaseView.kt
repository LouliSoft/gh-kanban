package com.ilizarraga.ghkanban.presentation.presenter

interface BaseView<T> {

    var mPresenter: T

}