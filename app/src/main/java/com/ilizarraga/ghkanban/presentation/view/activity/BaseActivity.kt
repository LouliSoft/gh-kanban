package com.ilizarraga.ghkanban.presentation.view.activity

import android.support.v7.app.AppCompatActivity

open class BaseActivity : AppCompatActivity()