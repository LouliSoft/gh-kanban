package com.ilizarraga.ghkanban.presentation.presenter.contract

import com.ilizarraga.ghkanban.domain.entity.Repository
import com.ilizarraga.ghkanban.presentation.presenter.BasePresenter
import com.ilizarraga.ghkanban.presentation.presenter.BaseView

interface MainContract {

    interface View : BaseView<Presenter> {

        fun showRepositoriesList(repositoriesList: List<Repository>)

        fun showError(error: Throwable)

    }

    interface Presenter : BasePresenter {

        fun getRepositoriesList()

        fun getKanbanList()
    }
}